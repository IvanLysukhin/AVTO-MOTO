export const TabType = {
  SPEC: 'SPEC',
  REVIEWS: 'REVIEWS',
  CONTACTS: 'CONTACTS',
};

export const REVIEWS = 'reviews';

export const ActionType = {
  ADD_REVIEW: 'ADD_REVIEW',
  LOAD_REVIEW: 'LOAD_REVIEW',
};
